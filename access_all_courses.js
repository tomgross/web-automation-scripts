/*
 * Testcafe script to access all course resources (to verify migrations)
 *
 * Getting started: https://devexpress.github.io/testcafe/documentation/getting-started/
 *
 * Run:
 *
 * USER='adminuser' PASS='topsecret' /usr/local/bin/testcafe chrome access_all_courses.js
 *
 */
import xlsx from 'node-xlsx';
import { Selector } from 'testcafe';
import { Role } from 'testcafe';

// Config
const server = 'https://exp.olat.uzh.ch';
const dataFile = "./data/courses.xlsx";
const excelSheetName = "Sheet1";

// Selectors
const searchButton = Selector('.btn-primary').withText('Suchen');
const typeDropdown = Selector('.dropdown-toggle').withText('Alle anzeigen').nth(0);
const courseType = Selector('input').withAttribute('data-value', 'Kurs').parent();

fixture `Getting Started`
    .page `${server}/auth/MyCoursesSite/0`;


const olatUser = Role('${server}/dmz/', async t => {
	await t
        .click('.o_icon_provider_olat')
		.typeText('#o_fiooolat_login_name', process.env.USER)
		.typeText('#o_fiooolat_login_pass', process.env.PASS)
		.click('#o_fiooolat_login_button');
}, { preserveUrl: true });

// get data from excel
// see https://dev-tester.com/data-driven-testing-in-testcafe-part-2-csv-and-excel/
const excelFile = xlsx.parse(dataFile);
const excelSheet = excelFile.find(sheets => sheets.name == excelSheetName);
const excelSheetData = excelSheet.data;
const headers = excelSheetData.shift();
const userData = excelSheetData.map((row) => {
	const resource = {}
	row.forEach((data, idx) => resource[headers[idx]] = data);
	return resource;
});

userData.forEach(resource => {
	test(`Accessed: ${resource.title}`, async t => {
		await t
			.click('.o_icon_provider_olat')
			.typeText('#o_fiooolat_login_name', process.env.USER)
			.typeText('#o_fiooolat_login_pass', process.env.PASS)
			.click('#o_fiooolat_login_button')
			.click(Selector('a').withText('Autorenbereich'))
			.click(Selector('a').withText('Suchmaske'))
			.click('.o_sel_flexi_extendedsearch')
			.click('.o_sel_repo_search_displayname')
			.pressKey('ctrl+a delete')
			.typeText('.o_sel_repo_search_displayname', '"' + resource.title + '"')
			.click(typeDropdown)
			.click(courseType)
			.click(searchButton)		
			// title of test
			.click(Selector('.o_coursetable tbody').find('tr').nth(0).find('td').nth(3).child('a'));


		if (await Selector('h1').withText('Ups, da ging was schief!').exists) {
			console.log('Failed: RedScreen!');
			await t
				.click(Selector('a').withText('Zurück'))
				.useRole(olatUser)
				.click(Selector('a').withText('Abbrechen'));
		} 
		await t
			.click('#o_sel_navbar_my_menu_caret')
			.click('a.o_logout');
	});
});